Name:		credmonger
Version:	0.1
Release:	1%{?dist}
Summary:	Maintains Kerberos credential caches for other processes

Group:		System Environment/Daemons
License:	MIT
URL:		https://badvocacy.net/
Source0:	credmonger-%{version}.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	krb5-devel
Requires(post):	chkconfig
Requires(preun):	chkconfig, initscripts
Requires(postun):	initscripts

%description
The credmonger daemon creates and refreshes Kerberos credential cache
files on behalf of other processes and users, which are typically daemons
which need to authenticate to other services using Kerberos.

%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_initrddir}
install -m755 src/credmonger.init $RPM_BUILD_ROOT/%{_initrddir}/credmonger

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/chkconfig --add credmonger

%preun
if [ "$1" -eq 0 ] ; then
	/sbin/service credmonger stop > /dev/null 2>&1 || :
	/sbin/chkconfig --del credmonger
fi
exit 0

%postun
if [ "$1" -ge 1 ] ; then
	/sbin/service credmonger condrestart > /dev/null 2>&1 || :
fi
exit 0

%files
%defattr(-,root,root,-)
%doc LICENSE src/credmonger.sample
%dir %{_sysconfdir}/credmonger.d
%config(noreplace) %{_sysconfdir}/credmonger.d/*
%{_initrddir}/credmonger
%{_sbindir}/*
%{_mandir}/*/*

%changelog
* Wed Apr  1 2009 Nalin Dahyabhai <nalin@redhat.com> 0.0-1
- initial package
